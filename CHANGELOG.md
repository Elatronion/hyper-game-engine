# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
- Mouse capturing
- Loading default shaders with the resource manager
- Loading default textures with the resource manager ("HGE DEFAULT NORMAL")
- Linked lists
- Batch rendering

## [2.0.0] - 01/15/2021
### Added
- Framebuffers
- Meshes
- OBJ Loader
- Audio Engine using OpenAL
- Quaternions
- Function hgeVec[n] to easily create hge_vec[n]
-  Lights
- New shader pipeline
- **hgeResourcesLoadDefaults**
- Mesh "HGE QUAD" is loaded by default and used to render sprites and spritesheets
- Vector math for hge_vec2
- Flag to initialize Audio Engine
- **hgeCreateCamera**

### Changed
- Every **hge_camera** has it's own framebuffer
- Rendering will be done for every **hge_camera** (component "camera")
- **hgeResourcesQueryTexture** will return null if no texture was found
- **SpriteRenderingSystem** was renamed to **hge_system_sprite**
- **SpriteSheetSystem** was renamed to **hge_system_spritesheet**
- **FollowTarget** was renamed to **hge_system_follower**
- **FreeCam** was renamed to **hge_system_noclip**
- Rendering sprites is done with **hge_material** over **hge_texture**
- All default component names are now lowercase
- Shader "sprite_shader" has been renamed to "basic"
- Shader "framebuffer_shader" has been renamed to "framebuffer"
- hgeMathTypeFunctions have been renamed to hgeTypeFunctions
- Tag "ActiveCamera" has been renamed to "active"

### Deprecated
- **hgeResourcesGetShaderArray** will be replaced with a linked list
- **hgeResourcesGetMeshArray** will be replaced with a linked list

### Removed
- **CameraSystem**
- **SpriteRenderingSystem**
- **SpriteSheetSystem**
- **FreeCam**

### Fixed
- **TERMINAL_COLORCODE_COLOR** fixed for non-Linux platforms
- Entities and systems are now freed
- Proper files are being included to prevent most warnings when compiling


## [1.2.0] - 09/02/2020
### Added
- Basic networking engine (using enet)

### Changed
- Start using flags for engine initialization

### Fixed
- Large number of entities or systems  no longer crash the engine

## [1.1.0] - 07/20/2020
### Added
- ECS now has function "hgeDestroyEntity" to destroy entities
- This CHANGELOG file to hopefully serve as a highlight to notable changes

### Changed
- Start using "libhypergameengine" over "libHyperGameEngine"
- Installer now uses "/usr/include/HGE/" to store library includes
- Glad moved from "HGE_RenderingEngine.h" to "HGE_RenderingEngine.c"
- Start using "long int" as mask for "hgeClear" over "GLbitfield"

### Fixed
- FPS Cap now caps frames
- Window's input array loops now loop every key
- Crash when loading shaders that didn't exist

## [1.0.0] - 06/22/2020
### Added
- Base
