#!/bin/bash

lib_file=libhypergameengine.so
cpu_arch=x86_64
lib_folder=lib

if [ "$1" != "" ]
then
	cpu_arch=$1
fi

if [ $cpu_arch == x86_64 ] && [ -d "/lib64/" ]
then
	lib_folder=lib64
fi

echo "Installing $lib_file for $cpu_arch in $lib_folder"

sudo mkdir -p /usr/include/HGE && find ./src/ -iname *.h -exec sudo cp "{}" /usr/include/HGE/  \;
sudo chown root ./build/linux/$cpu_arch/$lib_file && \
sudo chgrp root ./build/linux/$cpu_arch/$lib_file && \
sudo mv ./build/linux/$cpu_arch/$lib_file /usr/$lib_folder/
