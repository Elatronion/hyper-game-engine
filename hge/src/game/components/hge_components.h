#ifndef HGE_COMPONENTS_H
#define HGE_COMPONENTS_H

#include "HGE_Core.h"
#include "HGE_RenderingEngine.h"

typedef struct {} tag_component;

typedef struct {
	float pitch;
	float yaw;
	float roll;
} orientation_component;

typedef struct {
	hge_vec3* target_pos;
	float speed;
	bool lock_x, lock_y, lock_z;
} follow_component;

typedef struct {
	float speed;
} freemove_component;

// For Sprite sheets
typedef struct {
	float time;
	int FPS;
	hge_vec2 resolution;
	hge_vec2 frame;
	int num_frames;
	bool flipped;
	bool playing;
	hge_material spritesheet_material;
} spritesheet_component;

void hge_system_follower(hge_entity* entity, hge_transform* transform, follow_component* follow);
void hge_system_noclip(hge_entity* entity, hge_camera* camera, freemove_component* freemove, hge_transform* transform);

void hge_system_sprite(hge_entity* entity, hge_transform* transform, hge_texture* sprite);
void hge_system_spritesheet(hge_entity* entity, hge_transform* transform, spritesheet_component* spritesheet);

void hgeAddBaseSystems();

#define HGE_CAMERA_ORTHOGRAPHIC 0x1
#define HGE_CAMERA_FILTER_LINEAR 0x2
hge_entity* hgeCreateCamera(hge_transform transform, hge_vec2 resolution, float fov, float near, float far, unsigned int flags);

#endif
