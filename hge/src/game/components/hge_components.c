#include "hge_components.h"

void hge_system_noclip(hge_entity* entity, hge_camera* camera, freemove_component* freemove, hge_transform* transform) {
	float speed = freemove->speed*hgeDeltaTime();
	if (hgeInputGetKey(HGE_KEY_D))
		transform->position.x = transform->position.x + speed;
	if (hgeInputGetKey(HGE_KEY_A))
		transform->position.x = transform->position.x - speed;
	if (hgeInputGetKey(HGE_KEY_W))
		transform->position.y = transform->position.y + speed;
	if (hgeInputGetKey(HGE_KEY_S))
		transform->position.y = transform->position.y - speed;

	if (hgeInputGetKey(HGE_KEY_UP))
		transform->position.z = transform->position.z - speed;
	if (hgeInputGetKey(HGE_KEY_DOWN))
		transform->position.z = transform->position.z + speed;
}

void hge_system_sprite(hge_entity* entity, hge_transform* transform, hge_texture* sprite) {
	hge_material material = { *sprite, hgeResourcesQueryTexture("HGE DEFAULT NORMAL") };
	hgeRenderSprite(hgeResourcesQueryShader("basic"), material, *transform);
}

void hge_system_spritesheet(hge_entity* entity, hge_transform* transform, spritesheet_component* spritesheet) {

	// Update Frame X Relative To FPS
	spritesheet->time += hgeDeltaTime();
	if (spritesheet->time >= 1.0f/spritesheet->FPS) {
			spritesheet->frame.x++;
		if(spritesheet->frame.x > spritesheet->num_frames)
			spritesheet->frame.x = 0;
		spritesheet->time = 0;
	}

	// Flip By Reversing X Scale
	hge_vec3 rendering_scale = transform->scale;
	if(spritesheet->flipped)
		rendering_scale.x = -rendering_scale.x;

	hge_transform rendered_transform = *transform;
	rendered_transform.scale = rendering_scale;
	hgeRenderSpriteSheet(hgeResourcesQueryShader("basic"), spritesheet->spritesheet_material, rendered_transform, spritesheet->resolution, spritesheet->frame);
}

void hge_system_follower(hge_entity* entity, hge_transform* transform, follow_component* follow) {
	if(!follow->lock_x) transform->position.x += (follow->target_pos->x - transform->position.x) * follow->speed * hgeDeltaTime();
	if(!follow->lock_y) transform->position.y += (follow->target_pos->y - transform->position.y) * follow->speed * hgeDeltaTime();
	if(!follow->lock_z) transform->position.z += (follow->target_pos->z - transform->position.z) * follow->speed * hgeDeltaTime();
}

void hgeAddBaseSystems() {
  hgeAddSystem(hge_system_noclip, 3, "camera", "noclip", "transform");
	hgeAddSystem(hge_system_follower, 2, "transform", "follower");
	hgeAddSystem(hge_system_sprite, 2, "transform", "sprite");
	hgeAddSystem(hge_system_spritesheet, 2, "transform", "spritesheet");
}

hge_entity* hgeCreateCamera(hge_transform transform, hge_vec2 resolution, float fov, float near, float far, unsigned int flags) {
	hge_entity* camera_entity = hgeCreateEntity();
	hge_camera cam = {
		(flags & HGE_CAMERA_ORTHOGRAPHIC),
		fov,
		resolution.x, resolution.y,
		(float)resolution.x / (float)resolution.y,
		near, far
	};
	cam.framebuffer = hgeGenerateFramebuffer(resolution.x, resolution.y, false, (flags & HGE_CAMERA_FILTER_LINEAR));
	hgeAddComponent(camera_entity, hgeCreateComponent("camera", &cam, sizeof(cam)));
	hgeAddComponent(camera_entity, hgeCreateComponent("transform", &transform, sizeof(transform)));
	return camera_entity;
}
