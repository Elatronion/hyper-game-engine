#include "HGE_ECS.h"
#include "HGE_Log.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

hge_world active_world;

void hgeDestroyEntity(hge_entity* entity) {
	// find index of entity
	int index = -1;
	for(int e = 0; e < active_world.NUM_ENTITIES; e++) {
		if(active_world.entities[e] == entity) {
			index = e;
			break;
		}
	}

	// error code if nothing was found
	if(index == -1) {
		HGE_WARNING("could not delete entity, not found!");
		return;
	}

	// free memory
	for(int i = 0; i < entity->numComponents; i++) {
		free(entity->components[i].data);
		entity->components[i].data = 0;
	}
	free(entity);
	entity = NULL;

	// delete entity from array
	for (int i = index; i < active_world.NUM_ENTITIES-1; ++i)
		active_world.entities[i] = active_world.entities[i + 1]; // copy next element left

	active_world.NUM_ENTITIES--;
}

hge_entity* hgeCreateEntity() {
	hge_entity* entity = malloc(sizeof(*entity));
	entity->numComponents = 0;
	active_world.entities[active_world.NUM_ENTITIES] = entity;
	active_world.NUM_ENTITIES++;
	return entity;
}

hge_component hgeCreateComponent(const char* name, void* componentData, size_t size) {
	hge_component component;
	component.name = name;
	component.data = malloc(size);
	memcpy ( component.data, componentData, size );
	return component;
}

int hgeQuery(hge_entity* entity, const char* component_name) {
	int found_index = -1;
	for(int i = 0; i < entity->numComponents; i++) {
		if(!strcmp(entity->components[i].name, component_name)) {
			found_index = i;
			break;
		}
	}
	return found_index;
}

hge_entity* hgeQueryEntity(int argc, ...) {

	hge_entity* queried_entity = NULL;
	int numFoundComponents = 0;

	for(int e = 0; e < active_world.NUM_ENTITIES; e++) {
		va_list args;
		va_start(args, argc);

		for(int i = 0; i < argc; i++) {
			const char* arg = va_arg(args, const char*);
			int c_id = hgeQuery(active_world.entities[e], arg);
			if(c_id == -1) continue;
			numFoundComponents++;
		}
		va_end(args);
		if(numFoundComponents >= argc) {
			queried_entity = active_world.entities[e];
			break;
		}
	}

	return queried_entity;
}

hge_ecs_request hgeECSRequest(int argc, ...) {

		hge_ecs_request request;
		request.NUM_ENTITIES = 0;

		for(int e = 0; e < active_world.NUM_ENTITIES; e++) {
			int numFoundComponents = 0;
			va_list args;
			va_start(args, argc);

			for(int i = 0; i < argc; i++) {
				const char* arg = va_arg(args, const char*);
				int c_id = hgeQuery(active_world.entities[e], arg);
				if(c_id == -1) continue;
				numFoundComponents++;
			}
			va_end(args);
			if(numFoundComponents >= argc) {
				request.entities[request.NUM_ENTITIES] = active_world.entities[e];
				request.NUM_ENTITIES++;
			}
		}

		return request;
}

void hgeAddComponent(hge_entity* entity, hge_component component) {
	entity->components[entity->numComponents] = component;
	entity->numComponents++;
}

void hgeAddSystem(void (*function)(), int argc, ...) {
	printf("Function [%p] will be called for all entities with:\n", function);
	hge_system system;
	system.argc = argc;
	system.function = function;

	va_list args;
	va_start(args, argc);
	for(int i = 0; i < argc; i++) {
		const char* arg = va_arg(args, const char*);
		printf("\t%s\n", arg);
		system.dependentComponents[i] = arg;
	}
	va_end(args);

	active_world.systems[active_world.NUM_SYSTEMS] = system;
	active_world.NUM_SYSTEMS++;
}

// This Code Is Complete Trash, Please Fix.
// We could pass an array as a parameter to the function, but that would make it less fun to use.
// I'd love it if I could simply just pass the proper ammount of components... :(
void hgeWorldUpdate() {
	for(int i = 0; i < active_world.NUM_SYSTEMS; i++) {
		for(int e = 0; e < active_world.NUM_ENTITIES; e++) {
			bool run_system = true;
			int c_id = -1;
			void* component_pointer1 = NULL;
			void* component_pointer2 = NULL;
			void* component_pointer3 = NULL;
			void* component_pointer4 = NULL;
			void* component_pointer5 = NULL;
			void* component_pointer6 = NULL;
			void* component_pointer7 = NULL;
			void* component_pointer8 = NULL;
			void* component_pointer9 = NULL;
			void* component_pointer10 = NULL;
			switch(active_world.systems[i].argc) {
				case 10:
					c_id = hgeQuery(active_world.entities[e], active_world.systems[i].dependentComponents[9]);
					if(c_id == -1) run_system = false;
					component_pointer10 = active_world.entities[e]->components[c_id].data;
				case 9:
					c_id = hgeQuery(active_world.entities[e], active_world.systems[i].dependentComponents[8]);
					if(c_id == -1) run_system = false;
					component_pointer9 = active_world.entities[e]->components[c_id].data;
				case 8:
					c_id = hgeQuery(active_world.entities[e], active_world.systems[i].dependentComponents[7]);
					if(c_id == -1) run_system = false;
					component_pointer8 = active_world.entities[e]->components[c_id].data;
				case 7:
					c_id = hgeQuery(active_world.entities[e], active_world.systems[i].dependentComponents[6]);
					if(c_id == -1) run_system = false;
					component_pointer7 = active_world.entities[e]->components[c_id].data;
				case 6:
					c_id = hgeQuery(active_world.entities[e], active_world.systems[i].dependentComponents[5]);
					if(c_id == -1) run_system = false;
					component_pointer6 = active_world.entities[e]->components[c_id].data;
				case 5:
					c_id = hgeQuery(active_world.entities[e], active_world.systems[i].dependentComponents[4]);
					if(c_id == -1) run_system = false;
					component_pointer5 = active_world.entities[e]->components[c_id].data;
				case 4:
					c_id = hgeQuery(active_world.entities[e], active_world.systems[i].dependentComponents[3]);
					if(c_id == -1) run_system = false;
					component_pointer4 = active_world.entities[e]->components[c_id].data;
				case 3:
					c_id = hgeQuery(active_world.entities[e], active_world.systems[i].dependentComponents[2]);
					if(c_id == -1) run_system = false;
					component_pointer3 = active_world.entities[e]->components[c_id].data;
				case 2:
					c_id = hgeQuery(active_world.entities[e], active_world.systems[i].dependentComponents[1]);
					if(c_id == -1) run_system = false;
					component_pointer2 = active_world.entities[e]->components[c_id].data;
				case 1:
					c_id = hgeQuery(active_world.entities[e], active_world.systems[i].dependentComponents[0]);
					if(c_id == -1) run_system = false;
					component_pointer1 = active_world.entities[e]->components[c_id].data;
				default:
					break;
			}
			if(run_system)
				active_world.systems[i].function(active_world.entities[e], component_pointer1, component_pointer2, component_pointer3, component_pointer4, component_pointer5, component_pointer6, component_pointer7, component_pointer8, component_pointer9, component_pointer10);
		}
	}
}

void hgePrintECSInfo() {
	for(int e = 0; e < active_world.NUM_ENTITIES; e++) {
		printf("Entity[%p]:\n", active_world.entities[e]);
		for(int i = 0; i < active_world.entities[e]->numComponents; i++) {
			hge_component comp = active_world.entities[e]->components[i];
			printf("\tComponent[%p] '%s'\n", comp.data, comp.name);
		}
	}
}

void hgeECSInit() {
	HGE_LOG("initializing ECS engine");
	// Prevent stack overflow with large arrays using malloc
	active_world.systems = (hge_system*)malloc(HGE_MAX_ECS_ENTITIES * sizeof(hge_system));
	active_world.entities = (hge_entity**)malloc(HGE_MAX_ECS_ENTITIES * sizeof(hge_entity*));
	HGE_SUCCESS("initialized ECS engine");
}

void hgeECSCleanUp() {
	for(int e = 0; e < active_world.NUM_ENTITIES; e++) {
		for(int i = 0; i < active_world.entities[e]->numComponents; i++) {
			free(active_world.entities[e]->components[i].data);
			active_world.entities[e]->components[i].data = 0;
		}
		free(active_world.entities[e]);
		active_world.entities[e] = NULL;
	}
	free(active_world.entities);
	free(active_world.systems);
}
