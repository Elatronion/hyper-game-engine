#include "HGE_Quaternion.h"
#include "HGE_Math3D.h"

hge_quaternion hgeQuaternion(float x, float y, float z, float w) {
  hge_quaternion quaternion = { x, y, z, w };
  return quaternion;
}

hge_quaternion hgeQuaternionInitRotation(hge_vec3 axis, float angle) {
  float sinHalfAngle = (float)sin(angle / 2);
  float cosHalfAngle = (float)cos(angle / 2);

  return hgeQuaternion(
    axis.x * sinHalfAngle,
    axis.y * sinHalfAngle,
    axis.z * sinHalfAngle,
    cosHalfAngle
  );
}

float hgeQuaternionLength(hge_quaternion quaternion) {
  return (float) sqrt(
    quaternion.x * quaternion.x +
    quaternion.y * quaternion.y +
    quaternion.z * quaternion.z +
    quaternion.w * quaternion.w
  );
}

hge_quaternion hgeQuaternionNormalize(hge_quaternion quaternion) {
  float length = hgeQuaternionLength(quaternion);
  hge_quaternion new_quaternion = {
    quaternion.x / length,
    quaternion.y / length,
    quaternion.z / length,
    quaternion.w / length
  };
  return new_quaternion;
}

hge_quaternion hgeQuaternionConjugate(hge_quaternion quaternion) {
  return hgeQuaternion(-quaternion.x, -quaternion.y, -quaternion.z, quaternion.w);
}

hge_quaternion hgeQuaternionMul(hge_quaternion A, hge_quaternion B) {
  float w_ = A.w * B.w - A.x * B.x - A.y * B.y - A.z * B.z;
  float x_ = A.x * B.w + A.w * B.x + A.y * B.z - A.z * B.y;
  float y_ = A.y * B.w + A.w * B.y + A.z * B.x - A.x * B.z;
  float z_ = A.z * B.w + A.w * B.z + A.x * B.y - A.y * B.x;
  hge_quaternion new_quaternion = { x_, y_, z_, w_ };
  return new_quaternion;
}

hge_quaternion hgeQuaternionMulVec3(hge_quaternion A, hge_vec3 B) {
  float w_ = -A.x * B.x - A.y * B.y - A.z * B.z;
  float x_ =  A.w * B.x + A.y * B.z - A.z * B.y;
  float y_ =  A.w * B.y + A.z * B.x - A.x * B.z;
  float z_ =  A.w * B.z + A.x * B.y - A.y * B.x;
  hge_quaternion new_quaternion = { x_, y_, z_, w_ };
  return new_quaternion;
}

hge_mat4 hgeQuaternionRotationMat4(hge_quaternion quaternion) {

  //hge_vec3 forward = hgeQuaternionGetFoward(quaternion);
  //hge_vec3 up = hgeQuaternionGetUp(quaternion);
  //hge_vec3 right = hgeQuaternionGetRight(quaternion);

  hge_vec3 forward = hgeVec3(2.0f * (quaternion.x * quaternion.z - quaternion.w * quaternion.y), 2.0f * (quaternion.y * quaternion.z + quaternion.w * quaternion.x), 1.0f - 2.0f * (quaternion.x * quaternion.x + quaternion.y * quaternion.y));
	hge_vec3 up = hgeVec3(2.0f * (quaternion.x * quaternion.y + quaternion.w * quaternion.z), 1.0f - 2.0f * (quaternion.x * quaternion.x + quaternion.z * quaternion.z), 2.0f * (quaternion.y * quaternion.z - quaternion.w * quaternion.x));
	hge_vec3 right = hgeVec3(1.0f - 2.0f * (quaternion.y * quaternion.y + quaternion.z * quaternion.z), 2.0f * (quaternion.x * quaternion.y - quaternion.w * quaternion.z), 2.0f * (quaternion.x * quaternion.z + quaternion.w * quaternion.y));
  return hgeMat4InitRotation(forward, up, right);
}

// TODO: Verify if all Get*Direction* functions are actually functional...

hge_vec3 hgeQuaternionGetFoward(hge_quaternion quaternion) {
  return hgeVec3(
    2.0f * (quaternion.x*quaternion.z - quaternion.w*quaternion.y),
    2.0f * (quaternion.y*quaternion.z + quaternion.w*quaternion.x),
    1.0f - 2.0f * (quaternion.x*quaternion.x + quaternion.y*quaternion.y)
  );
}

hge_vec3 hgeQuaternionGetBack(hge_quaternion quaternion) {
  return hgeVec3(
    -2.0f * (quaternion.x*quaternion.z - quaternion.w*quaternion.y),
    -2.0f * (quaternion.y*quaternion.z + quaternion.w*quaternion.x),
    -(1.0f - 2.0f * (quaternion.x*quaternion.x + quaternion.y*quaternion.y))
  );
}

hge_vec3 hgeQuaternionGetUp(hge_quaternion quaternion) {
  return hgeVec3(
    2.0f * (quaternion.x * quaternion.y + quaternion.w * quaternion.z),
    1.0f - 2.0f * (quaternion.x * quaternion.x + quaternion.z * quaternion.z),
    2.0f * (quaternion.y * quaternion.z - quaternion.w * quaternion.x)
  );
}

hge_vec3 hgeQuaternionGetDown(hge_quaternion quaternion) {
  return hgeVec3(
    -2.0f * (quaternion.x * quaternion.y + quaternion.w * quaternion.z),
    -(1.0f - 2.0f * (quaternion.x * quaternion.x + quaternion.z * quaternion.z)),
    -2.0f * (quaternion.y * quaternion.z - quaternion.w * quaternion.x)
  );
}

// Reversed Front & Back
hge_vec3 hgeQuaternionGetLeft(hge_quaternion quaternion) {
  return hgeVec3(
    1.0f - 2.0f * (quaternion.y * quaternion.y + quaternion.z * quaternion.z),
    2.0f * (quaternion.x * quaternion.y - quaternion.w * quaternion.z),
    2.0f * (quaternion.x * quaternion.z + quaternion.w * quaternion.y)
  );
}

hge_vec3 hgeQuaternionGetRight(hge_quaternion quaternion) {
  return hgeVec3(
    -(1.0f - 2.0f * (quaternion.y * quaternion.y + quaternion.z * quaternion.z)),
    -2.0f * (quaternion.x * quaternion.y - quaternion.w * quaternion.z),
    -2.0f * (quaternion.x * quaternion.z + quaternion.w * quaternion.y)
  );
}
