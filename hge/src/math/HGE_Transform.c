#include "HGE_Transform.h"

hge_transform hgeTransform(hge_vec3 position, hge_vec3 scale, hge_quaternion rotation) {
  hge_transform transform;
  transform.position = position;
  transform.scale = scale;
  transform.rotation = rotation;
  return transform;
}
