#include "HGE_Math3D.h"
#include <stdio.h>

float hgeRadians(float degrees) {
  return ((2 * M_PI) * degrees) / 360;
}
