#ifndef HGE_QUATERNION
#define HGE_QUATERNION
#include "HGE_MathStructures.h"

hge_quaternion hgeQuaternion(float x, float y, float z, float w);

hge_quaternion hgeQuaternionInitRotation(hge_vec3 axis, float angle);

float hgeQuaternionLength(hge_quaternion quaternion);

hge_quaternion hgeQuaternionNormalize(hge_quaternion quaternion);

hge_quaternion hgeQuaternionConjugate(hge_quaternion quaternion);

hge_quaternion hgeQuaternionMul(hge_quaternion A, hge_quaternion B);

hge_quaternion hgeQuaternionMulVec3(hge_quaternion A, hge_vec3 B);

hge_mat4 hgeQuaternionRotationMat4(hge_quaternion quaternion);

hge_vec3 hgeQuaternionGetFoward(hge_quaternion quaternion);

hge_vec3 hgeQuaternionGetBack(hge_quaternion quaternion);

hge_vec3 hgeQuaternionGetUp(hge_quaternion quaternion);

hge_vec3 hgeQuaternionGetDown(hge_quaternion quaternion);

hge_vec3 hgeQuaternionGetRight(hge_quaternion quaternion);

hge_vec3 hgeQuaternionGetLeft(hge_quaternion quaternion);

#endif
