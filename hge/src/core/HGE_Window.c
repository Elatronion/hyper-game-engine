#include "HGE_Window.h"
#include "HGE_Log.h"
#include "HGE_Input.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "HGE_RenderingEngine.h"

#include "HGE_Core.h"

unsigned int SCREEN_WIDTH = 0;
unsigned int SCREEN_HEIGHT = 0;

unsigned int hgeWindowWidth(){ return SCREEN_WIDTH; }
unsigned int hgeWindowHeight(){ return SCREEN_HEIGHT; }

void glfw_framebuffer_size_callback(GLFWwindow* glfw_window, int width, int height)
{
	hge_entity* active_camera_entity = hgeQueryEntity(1, "active");
	hge_camera* active_camera = active_camera_entity->components[hgeQuery(active_camera_entity, "camera")].data;
	active_camera->width = width;
	active_camera->height = height;
	active_camera->framebuffer = hgeGenerateFramebuffer(width, height, false, false);
	SCREEN_WIDTH = width;
	SCREEN_HEIGHT = height;
}

GLFWwindow* glfw_window;

void glfw_key_callback(GLFWwindow* glfw_window, int key, int scancode, int action, int mods)
{
	switch(action) {
		case GLFW_PRESS:
			hgeInputSetKey(key, true);
			hgeInputSetKeyDown(key, true);
			hgeInputSetKeyUp(key, false);
			break;
		case GLFW_RELEASE:
			hgeInputSetKey(key, false);
			hgeInputSetKeyDown(key, false);
			hgeInputSetKeyUp(key, true);
			break;
		default:
			break;
	}
}

void glfw_mousebutton_callback(GLFWwindow* glfw_window, int button, int action, int mods) {
	switch(action) {
		case GLFW_PRESS:
			hgeInputSetMouse(button, true);
			hgeInputSetMouseDown(button, true);
			hgeInputSetMouseUp(button, false);
			break;
		case GLFW_RELEASE:
			hgeInputSetMouse(button, false);
			hgeInputSetMouseDown(button, false);
			hgeInputSetMouseUp(button, true);
			break;
		default:
			break;
	}
}

void glfw_mouse_callback(GLFWwindow* glfw_window, double xpos, double ypos)
{
	hgeInputSetMouseX(xpos);
	hgeInputSetMouseY(ypos);
}

void hgeSwapBuffers() {
	glfwSwapBuffers(glfw_window);
}

void hgePollEvents() {
	for(int i = 0; i <= HGE_KEY_LAST; i++){
		hgeInputSetKeyDown(i, false);
		hgeInputSetKeyUp(i, false);
	}
	for(int i = 0; i <= HGE_MOUSE_LAST; i++){
		hgeInputSetMouseDown(i, false);
		hgeInputSetMouseUp(i, false);
	}
	glfwPollEvents();
}

int hgeCreateWindow(const char* title, int x, int y, int width, int height, uint8_t flags)
{
	HGE_LOG("creating window");
	SCREEN_WIDTH = width;
	SCREEN_HEIGHT = height;
	if (!glfwInit())
	{
		HGE_ERROR("failed to init GLFW");
		return 1;
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

	glfw_window = glfwCreateWindow(width, height, title, NULL, NULL);
	if(!glfw_window)
	{
		HGE_ERROR("failed to create GLFW window");
		glfwTerminate();
		return 1;
	}


	glfwMakeContextCurrent(glfw_window);
	glfwSetFramebufferSizeCallback(glfw_window, glfw_framebuffer_size_callback);

	if(flags & HGE_CAPTURE_CURSOR)
		glfwSetInputMode(glfw_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetCursorPosCallback(glfw_window, glfw_mouse_callback);
	glfwSetKeyCallback(glfw_window, glfw_key_callback);
	glfwSetMouseButtonCallback(glfw_window, glfw_mousebutton_callback);

	glfwSwapInterval(false);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		HGE_ERROR("failed to initialize GLAD");
		glfwTerminate();
		return 1;
	}

	HGE_SUCCESS("created window");
	return 0;
}

void hgeDestroyWindow() {
	glfwTerminate();
	HGE_LOG("window terminated");
}

bool hgeWindowIsClosedRequested() {
	return glfwWindowShouldClose(glfw_window);
}
