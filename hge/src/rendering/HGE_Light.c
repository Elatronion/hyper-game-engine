#include "HGE_Light.h"
#include "HGE_ResourceManager.h"
#include "HGE_Shader.h"
#include <stdio.h>

int light_id = 0;

hge_dirlight hgeDirLight(
    hge_vec3 ambient,
    hge_vec3 diffuse,
    hge_vec3 specular,
    hge_vec3 direction
) {
  hge_dirlight light;
  light.ambient = ambient;
  light.diffuse = diffuse;
  light.specular = specular;

  light.direction = direction;
  return light;
}

hge_pointlight hgePointLight(
    hge_vec3 ambient,
    hge_vec3 diffuse,
    hge_vec3 specular,
    float constant,
    float linear,
    float quadratic
) {
    hge_pointlight light;
    light.id = light_id;
    light_id++;
    if(light_id == HGE_MAX_POINT_LIGHTS) light_id = 0;
    light.ambient = ambient;
    light.diffuse = diffuse;
    light.specular = specular;

    light.constant = constant;
    light.linear =   linear;
    light.quadratic = quadratic;
    return light;
}

void hgeLightDataReset() {
  hge_shader light_shader = hgeResourcesQueryShader("basic");
  hgeUseShader(light_shader);

  hgeShaderSetVec3(light_shader, "dirLight.ambient", hgeVec3(0, 0, 0));
  hgeShaderSetVec3(light_shader, "dirLight.diffuse", hgeVec3(0, 0, 0));
  hgeShaderSetVec3(light_shader, "dirLight.specular", hgeVec3(0, 0, 0));
  hgeShaderSetVec3(light_shader, "dirLight.direction", hgeVec3(0, 0, 0));

  for(int i = 0; i < HGE_MAX_POINT_LIGHTS; i++) {
    int shader_index = i;
    char light_var[255];
    sprintf(light_var, "pointLights[%d].position", shader_index);
    hgeShaderSetVec3(light_shader, light_var, hgeVec3(0, 0, 0));

    sprintf(light_var, "pointLights[%d].ambient", shader_index);
    hgeShaderSetVec3(light_shader, light_var, hgeVec3(0, 0, 0));

    sprintf(light_var, "pointLights[%d].diffuse", shader_index);
    hgeShaderSetVec3(light_shader, light_var, hgeVec3(0, 0, 0));

    sprintf(light_var, "pointLights[%d].specular", shader_index);
    hgeShaderSetVec3(light_shader, light_var, hgeVec3(0, 0, 0));

    sprintf(light_var, "pointLights[%d].constant", shader_index);
    hgeShaderSetFloat(light_shader, light_var, 0.0f);

    sprintf(light_var, "pointLights[%d].linear", shader_index);
    hgeShaderSetFloat(light_shader, light_var, 0.0f);

    sprintf(light_var, "pointLights[%d].quadratic", shader_index);
    hgeShaderSetFloat(light_shader, light_var, 0.0f);
  }
}

void hge_system_dirlight(hge_entity* entity, hge_dirlight* light) {
  hge_shader light_shader = hgeResourcesQueryShader("basic");
  hgeUseShader(light_shader);

  hgeShaderSetVec3(light_shader, "dirLight.ambient", light->ambient);
  hgeShaderSetVec3(light_shader, "dirLight.diffuse", light->diffuse);
  hgeShaderSetVec3(light_shader, "dirLight.specular", light->specular);
  hgeShaderSetVec3(light_shader, "dirLight.direction", light->direction);
}

void hge_system_pointlight(hge_entity* entity, hge_vec3* position, hge_pointlight* light) {
  hge_shader light_shader = hgeResourcesQueryShader("basic");
  hgeUseShader(light_shader);

  int shader_index = light->id;
  char light_var[255];
  sprintf(light_var, "pointLights[%d].position", shader_index);
  hgeShaderSetVec3(light_shader, light_var, *position);

  sprintf(light_var, "pointLights[%d].ambient", shader_index);
	hgeShaderSetVec3(light_shader, light_var, light->ambient);

  sprintf(light_var, "pointLights[%d].diffuse", shader_index);
	hgeShaderSetVec3(light_shader, light_var, light->diffuse);

  sprintf(light_var, "pointLights[%d].specular", shader_index);
	hgeShaderSetVec3(light_shader, light_var, light->specular);

  sprintf(light_var, "pointLights[%d].constant", shader_index);
	hgeShaderSetFloat(light_shader, light_var, light->constant);

  sprintf(light_var, "pointLights[%d].linear", shader_index);
	hgeShaderSetFloat(light_shader, light_var, light->linear);

  sprintf(light_var, "pointLights[%d].quadratic", shader_index);
	hgeShaderSetFloat(light_shader, light_var, light->quadratic);
}
