#include "HGE_Shader.h"
#include "HGE_FileUtility.h"
#include "HGE_Log.h"

#include <glad/glad.h>

// TODO: Load Geometry Shader
hge_shader hgeLoadShader(const char* vertexPath, const char* geometryPath, const char* fragmentPath) {

  hge_shader shader;

  // 1. retrieve the vertex/fragment source code from filePath
  char* vShaderCode = hgeLoadFileAsString(vertexPath);
  char* fShaderCode = hgeLoadFileAsString(fragmentPath);

  if(vShaderCode == NULL || fShaderCode == NULL) {
    HGE_ERROR("None of the following shaders were found:\n\tVertex Shader: %s\n\tFragment Shader: %s\n",
              vertexPath, fragmentPath);
    return shader;
  }

  // 2. compile shaders
  unsigned int vertex, fragment;
  int success;
  char infoLog[512];

  // vertex Shader
  vertex = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertex, 1, (const GLchar**)&vShaderCode, NULL);
  glCompileShader(vertex);
  // print compile errors if any
  glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
  if(!success)
  {
      glGetShaderInfoLog(vertex, 512, NULL, infoLog);
      HGE_ERROR("SHADER::VERTEX::COMPILATION_FAILED");
      HGE_ERROR(infoLog);
  };

  // fragment Shader
  fragment = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragment, 1, (const GLchar**)&fShaderCode, NULL);
  glCompileShader(fragment);
  // print compile errors if any
  glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
  if(!success)
  {
      glGetShaderInfoLog(vertex, 512, NULL, infoLog);
      HGE_ERROR("SHADER::FRAGMENT::COMPILATION_FAILED");
      HGE_ERROR(infoLog);
  };

  // shader Program
  shader.id = glCreateProgram();
  glAttachShader(shader.id, vertex);
  glAttachShader(shader.id, fragment);
  glLinkProgram(shader.id);
  // print linking errors if any
  glGetProgramiv(shader.id, GL_LINK_STATUS, &success);
  if(!success)
  {
      glGetProgramInfoLog(shader.id, 512, NULL, infoLog);
      HGE_ERROR("SHADER::PROGRAM::LINKING_FAILED");
      HGE_ERROR(infoLog);
  }

  // delete the shaders as they're linked into our program now and no longer necessery
  glDeleteShader(vertex);
  glDeleteShader(fragment);

  free(vShaderCode);
  free(fShaderCode);

  return shader;
}

void hgeUseShader(hge_shader shader) {
  glUseProgram(shader.id);
}

void hgeShaderSetMatrix4(hge_shader shader, const char *name, hge_mat4 matrix) {
	glUniformMatrix4fv(glGetUniformLocation(shader.id, name), 1, GL_FALSE, &matrix.value[0][0]);
}

void hgeShaderSetBool(hge_shader shader, const char* name, bool value) {
    glUniform1i(glGetUniformLocation(shader.id, name), value);
}

void hgeShaderSetFloat(hge_shader shader, const char* name, float value) {
    glUniform1f(glGetUniformLocation(shader.id, name), value);
}

void hgeShaderSetVec3(hge_shader shader, const char *name, hge_vec3 vec3) {
  glUniform3f(glGetUniformLocation(shader.id, name), vec3.x, vec3.y, vec3.z);
}

void hgeShaderSetVec4(hge_shader shader, const char *name, hge_vec4 vec4){
  glUniform4f(glGetUniformLocation(shader.id, name), vec4.x, vec4.y, vec4.z, vec4.w);
}

void hgeShaderSetInt(hge_shader shader, const char* name, int value) {
  glUniform1i(glGetUniformLocation(shader.id, name), value);
}
