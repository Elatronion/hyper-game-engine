#ifndef HGE_RENDERING_ENGINE_H
#define HGE_RENDERING_ENGINE_H

#include "HGE_Math3D.h"
#include "HGE_Texture.h"
#include "HGE_Shader.h"
#include "HGE_Window.h"
#include "HGE_FrameBuffer.h"
#include "HGE_Light.h"
#include "HGE_Mesh.h"

typedef struct {
  bool isOrthographic;

  float fov;
  float width;
  float height;
  float aspect;
  float zNear;
  float zFar;
  hge_framebuffer framebuffer;
} hge_camera;

void hgeClearColor(float r, float g, float b, float a);
void hgeClear(long int mask);

void hgeRenderInit(hge_window window);

void hgeRenderFramebufferToScreen(hge_framebuffer framebuffer);

void hgeRenderMesh(hge_shader shader, hge_material material, hge_mesh mesh, hge_transform transform);
void hgeRenderSprite(hge_shader shader, hge_material material, hge_transform transform);
void hgeRenderSpriteSheet(hge_shader shader, hge_material material, hge_transform transform, hge_vec2 frame_resolution, hge_vec2 frame);
void hgeRenderQuad();

void hgeProcessCamera(hge_camera* camera, hge_transform* transform);
void hgeRenderFrame();

void hgeRenderingEngineCleanUp();

#endif
