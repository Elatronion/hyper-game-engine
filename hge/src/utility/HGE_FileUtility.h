#ifndef HGE_FILE_UTILITY_H
#define HGE_FILE_UTILITY_H
#include "HGE_Texture.h"
#include <stdbool.h>

char* hgeLoadFileAsString(const char* path);
hge_texture hgeLoadTexture(const char* path);
char* hgeLoadWAV(const char* fn, int* chan, int* samplerate, int* bps, int* size);
bool hgeIsBigEndian();

#endif
