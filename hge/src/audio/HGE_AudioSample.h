#ifndef HGE_AUDIO_SAMPLE_H
#define HGE_AUDIO_SAMPLE_H

typedef struct {
  int channel, sampleRate, bps, size;
	char* data;
	unsigned int sourceid, bufferid, format;
} hge_audiosample;

hge_audiosample hgeLoadAudioFile(const char* path);
void hgeDestroyAudioSample(hge_audiosample sample);

#endif
