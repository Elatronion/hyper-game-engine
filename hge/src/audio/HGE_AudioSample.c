#include "HGE_AudioSample.h"
#include "HGE_FileUtility.h"
#include "AL/al.h"
#include "AL/alc.h"
#include <stdlib.h>

hge_audiosample hgeLoadAudioFile(const char* path) {
  hge_audiosample sample;
  sample.data = hgeLoadWAV(path, &sample.channel, &sample.sampleRate, &sample.bps, &sample.size);
	alGenBuffers(1, &sample.bufferid);

	if (sample.channel == 1)
	{
		if (sample.bps == 8)
		{
			sample.format = AL_FORMAT_MONO8;
		}
		else
		{
			sample.format = AL_FORMAT_MONO16;
		}
	}
	else
	{
		if (sample.bps == 8)
		{
			sample.format = AL_FORMAT_STEREO8;
		}
		else
		{
			sample.format = AL_FORMAT_STEREO16;
		}
	}
	alBufferData(sample.bufferid, sample.format, sample.data, sample.size, sample.sampleRate);
	alGenSources(1, &sample.sourceid);
	alSourcei(sample.sourceid, AL_BUFFER, sample.bufferid);
  return sample;
}

void hgeDestroyAudioSample(hge_audiosample sample) {
  alDeleteSources(1, &sample.sourceid);
	alDeleteBuffers(1, &sample.bufferid);
  free(sample.data);
}
