#ifndef HGE_AUDIO_SOURCE_H
#define HGE_AUDIO_SOURCE_H
#include "HGE_AudioSample.h"

typedef struct {
  hge_audiosample sample;
} hge_audiosource;

void hgeAudioSourcePlay(hge_audiosource source);
void hgeAudioSourceStop(hge_audiosource source);

#endif
