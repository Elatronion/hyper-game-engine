#include "HGE_Input.h"

#define NUM_KEYS HGE_KEY_LAST+1
#define NUM_MOUSEBUTTONS HGE_MOUSE_LAST+1

bool inputs[NUM_KEYS] = {0};
bool downKeys[NUM_KEYS] = {0};
bool upKeys[NUM_KEYS] = {0};
bool mouseInput[NUM_MOUSEBUTTONS] = {0};
bool downMouse[NUM_MOUSEBUTTONS] = {0};
bool upMouse[NUM_MOUSEBUTTONS] = {0};
int  mouseX;
int  mouseY;

bool hgeInputGetKey(int keyCode)              { return inputs[keyCode]; }
bool hgeInputGetKeyDown(int keyCode)          { return downKeys[keyCode]; }
bool hgeInputGetKeyUp(int keyCode)            { return upKeys[keyCode]; }
bool hgeInputGetMouse(int keyCode)            { return mouseInput[keyCode]; }
bool hgeInputGetMouseDown(int keyCode)        { return downMouse[keyCode]; }
bool hgeInputGetMouseUp(int keyCode)          { return upMouse[keyCode]; }
hge_vec3 hgeInputGetMousePosition() {
  hge_vec3 pos = {mouseX, mouseY};
  return pos;
}

void hgeInputSetKey(int keyCode, bool value) { inputs[keyCode] = value; }
void hgeInputSetKeyDown(int keyCode, bool value) { downKeys[keyCode] = value; }
void hgeInputSetKeyUp(int keyCode, bool value) { upKeys[keyCode] = value; }
void hgeInputSetMouse(int keyCode, bool value) { mouseInput[keyCode] = value; }
void hgeInputSetMouseDown(int keyCode, bool value) { downMouse[keyCode] = value; }
void hgeInputSetMouseUp(int keyCode, bool value) { upMouse[keyCode] = value; }
void hgeInputSetMouseX(int value) { mouseX = value; }
void hgeInputSetMouseY(int value) { mouseY = value; }
